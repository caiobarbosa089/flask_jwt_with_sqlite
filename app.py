from flask import Flask, jsonify
from flask_restx import Api, Resource, fields
from flask_jwt_extended import JWTManager
from blacklist import BLACKLIST
# Lauches App and Api
from settings import *
# Resources
from resources.hotel import Hotels, Hotel
from resources.user import User, UserRegister, UserLogin, UserLogout
# Models
from models.hotel import HotelModel

# Config JWT with the app
jwt = JWTManager(app)

# Creates DB before the first requisition
@app.before_first_request
def create_db():
    database.create_all()
    HotelModel.load_base_data()

# --------------------
# -- Jwt Middleware --
# --------------------
# Check blacklist
@jwt.token_in_blacklist_loader
def check_blacklist(token):
    return token['jti'] in BLACKLIST

# Emit message on Unauthorized token
@jwt.revoked_token_loader
def invalid_token():
    return jsonify({'message': 'You have been logged out.'}), 401 # unauthorized

# -------------------
# ---- Resources ----
# -------------------
api.add_resource(Hotels)
api.add_resource(Hotel)
api.add_resource(User)
api.add_resource(UserRegister)
api.add_resource(UserLogin)
api.add_resource(UserLogout)

def create_app():
    return app

# Default Flask's init 
if __name__ == '__main__':
    from extensions import database
    # Lauches the api with the app
    database.init_app(app)
    app.run(debug=True, port=5000)
