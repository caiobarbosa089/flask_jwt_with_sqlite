from extensions import database

class HotelModel(database.Model):
    # Table's name
    __tablename__ = 'hotels'

    # Defines columns
    hotel_id = database.Column(database.String, primary_key=True)
    name = database.Column(database.String(80))
    stars = database.Column(database.Float(precision=1))
    daily = database.Column(database.Float(precision=2))
    city = database.Column(database.String(40))

    # Constructor
    def __init__(self, hotel_id, name, stars, daily, city):
        self.hotel_id = hotel_id
        self.name = name
        self.stars = stars
        self.daily = daily
        self.city = city

    # Return a dictionary
    def json(self):
        return {
            'hotel_id': self.hotel_id,
            'name': self.name,
            'stars': self.stars,
            'daily': self.daily,
            'city': self.city
        }

    # Find Hotel by id
    # The @Classmethod is called because won't need self data
    @classmethod
    def find_hotel(cls, hotel_id):
        hotel = cls.query.filter_by(hotel_id=hotel_id).first()
        if hotel:
            return hotel
        return None

    # Save Hotel
    def save_hotel(self):
        database.session.add(self)
        database.session.commit()

    # Update Hotel
    def update_hotel(self, name, stars, daily, city):
        self.name = name
        self.stars = stars
        self.daily = daily
        self.city = city

    # Delete Hotel
    def delete_hotel(self):
        database.session.delete(self)
        database.session.commit()

    # Load initial data
    # The @Classmethod is called because won't need self data
    @classmethod
    def load_base_data(cls):
        hoteis_default = [
            { 'name': 'A', 'stars': 4.6, 'daily': 75.50, 'city': 'Jundiaí' },
            { 'name': 'B', 'stars': 2.6, 'daily': 55.00, 'city': 'Campinas' },
            { 'name': 'C', 'stars': 4.7, 'daily': 68.39, 'city': 'Itupeva' },
            { 'name': 'D', 'stars': 2.3, 'daily': 70.57, 'city': 'Campo Limpo' },
            { 'name': 'E', 'stars': 4.1, 'daily': 79.50, 'city': 'Vinhedo' },
            { 'name': 'F', 'stars': 4.4, 'daily': 45.25, 'city': 'Valinhos' },
            { 'name': 'G', 'stars': 3.8, 'daily': 38.75, 'city': 'Paulinia' },
            { 'name': 'H', 'stars': 2.9, 'daily': 35.10, 'city': 'Jundiaí' },
            { 'name': 'I', 'stars': 4.7, 'daily': 125.50, 'city': 'Jundiaí' },
            { 'name': 'J', 'stars': 3.6, 'daily': 55.70, 'city': 'Campinas' },
            { 'name': 'K', 'stars': 4.5, 'daily': 76.00, 'city': 'São Paulo' },
            { 'name': 'L', 'stars': 6.8, 'daily': 78.00, 'city': 'Jundiaí' },
            { 'name': 'M', 'stars': 4.6, 'daily': 83.00, 'city': 'Jundiaí' },
        ]
        for num, data in enumerate(hoteis_default, start = 1):
            try:
                hotel = cls.query.filter_by(hotel_id=num).first()
                if hotel:
                    return None
                # Instantiate a new hotel
                hotel = HotelModel(num, **data)
                # Save new Hotel
                hotel.save_hotel()
            except:
                return None