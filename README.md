# flask-jwt-with-sqlite
This project aims to exemplify a RestAPI application using the **Python** programming language and the **Flask** library.

## Requirements
Be sure you have **python**, **pip** and **virtualenv** installed.

You can download easily **python** from the [official website](https://www.python.org/downloads/).

Now install the **pip** following the steps on the [official website](https://pip.pypa.io/en/stable/installing/).

Now install the **virtualenv** following the steps on the [official website](https://virtualenv.pypa.io/en/latest/installation.html).

After the installation you can open the terminal and test both python and pip.

- Run `python -v` to get python's respective version if the installation was fully OK.

- Run`pip -v` to get pip's respective version, the same way you test python.

If you have both messages you are ready to go!

The **pip** is used to install and manage project dependencies and run it by command line.

## How to use

Run `virtualenv ambvir --python=python3.7` to create virtual environment.
> **Note:** You can use any other python version.

Run `source ambvir/bin/activate` to start virtual environment.

Run `pip install -r requirements.txt` to install dependencies.

Run `python app.py` to start de project.

To deactive de virtual enviroment run`deactivate`.

## Structural organization 
```bash
├── models
│   ├── __init.py
│   ├── hotel.py
│   ├── user.py
├── resources
│   ├── __init.py
│   ├── hotel.py
│   ├── user.py
├── .env
├── app.py
├── blacklist.py
├── extensions.py
├── requirements.txt
└── settings.py
```
## Files detailment
#### .env
Environment variables.
#### app.py
Application entrypoint the main file.
#### blacklist.py
Revoked tokens.
#### extensions.py
Extensions already initialized.
#### requirements.txt
List of packages that are not part of the Python stdlib.
#### models/
In this layer, we define properties and methods of each model.
#### resources/
In this layer, we define all methods of http calls.