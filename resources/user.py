from flask_restx import Resource, reqparse, fields
from flask_jwt_extended import create_access_token, jwt_required, get_raw_jwt
from werkzeug.security import safe_str_cmp
from models.user import UserModel
from blacklist import BLACKLIST 
from settings import api

# Swagger namespaces
user_namespace      = api.namespace('users', description='User operations')
login_namespace     = api.namespace('login', description='Login operations')
logout_namespace    = api.namespace('logout', description='Logout operations')
register_namespace  = api.namespace('register', description='Register operations')

# Swagger models 
user_model = api.model('User', {
    'user_id': fields.Integer(),
    'login': fields.String()
})
login_model = api.model('Login', {
    'login': fields.String(),
    'password': fields.String()
})


# Request body
body_attributes = reqparse.RequestParser()
body_attributes.add_argument('login',    type=str, required=True, help="The field 'login' cannot be left blank.")
body_attributes.add_argument('password', type=str, required=True, help="The field 'password' cannot be left blank.")

'''
Route for /users/<int:user_id>
'''
@user_namespace.route('/<int:user_id>')
@api.doc(params={'user_id': 'User ID'}) # Params in swagger
class User(Resource):
    # Get User by id
    @user_namespace.marshal_list_with(user_model, mask='') # Response model in swagger
    def get(self, user_id):
        # Find User by id
        user = UserModel.find_user(user_id)
        if user:
            return user.json(), 200 #Ok
        return {'message': 'User not found.'}, 404 # Not found

    # Delete User
    # Necessary JWT
    @jwt_required
    def delete(self, user_id):
        # Find User by id
        user = UserModel.find_user(user_id)
        if user:
            # Delete User
            user.delete_user()
            return {'message': 'User deleted.'}, 200 # Ok
        return {'message': 'User not found.'}, 404 # Not found

'''
Route for /register
'''
@register_namespace.route('')
class UserRegister(Resource):
    # Post User
    @register_namespace.expect(login_model) # Model in swagger
    def post(self):
        # Casting request Body data
        data = body_attributes.parse_args()
        # Checks if it already exists
        if UserModel.find_by_login(data['login']):
            return {"message": "The login '{}' already exists.".format(data['login'])}, 400 #Bad Request
        # Instantiate a new User
        user = UserModel(**data)

        try:
            # Save new User
            user.save_user()
        except:
            # If theres errors
            return {"message": "An error ocurred trying to create user."}, 500 # Internal Server Error
        
        # If all goes well
        return {'message': 'User created successfully!'}, 201 # Created

'''
Route for /login
'''
@login_namespace.route('')
class UserLogin(Resource):
    # Post login    
    @classmethod # The @Classmethod is called because won't need self data
    @login_namespace.expect(login_model) # Model in swagger
    def post(cls):
        # Casting request Body data
        data = body_attributes.parse_args()
        # Find User by login
        user = UserModel.find_by_login(data['login'])

        # Checks passwords
        if user and safe_str_cmp(user.password, data['password']):
            token_de_acesso = create_access_token(identity=user.user_id)
            return {'access_token': token_de_acesso}, 200 #Ok

        # If passwords are different 
        return {'message': 'The username or password is incorrect.'}, 401 # Unauthorized

'''
Route for /logout
'''
@logout_namespace.route('')
class UserLogout(Resource):
    # Post logout
    @jwt_required # Necessary JWT
    def post(self):
        # Get JWT id
        jwt_id = get_raw_jwt()['jti'] # JWT Token Identifier
        # Add JWT id in the BLACKLIST
        BLACKLIST.add(jwt_id)
        return {'message': 'Logged out successfully!'}, 200 # Ok
