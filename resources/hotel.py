from flask_restx import Resource, reqparse, fields
from flask_jwt_extended import jwt_required
from models.hotel import HotelModel
from settings import api
import sqlite3

# Swagger namespace
hotels_namespace    = api.namespace('hotels', description='Hotels operations')
hotel_namespace     = api.namespace('hotel', description='Hotel operations')

# Swagger models 
hotel_model = api.model('Hotel', {
    'hotel_id': fields.Integer(),
    'name': fields.String(),
    'stars': fields.String(),
    'daily': fields.String(),
    'city': fields.String()
})

# Params url
path_params = reqparse.RequestParser()
path_params.add_argument('city', type=str)
path_params.add_argument('star_min', type=float)
path_params.add_argument('star_max', type=float)
path_params.add_argument('daily_min', type=float)
path_params.add_argument('daily_max', type=float)
path_params.add_argument('limit', type=float)
path_params.add_argument('offset', type=float)

# Request body
body_attributes = reqparse.RequestParser()
body_attributes.add_argument('name', type=str, required=True, help="The field 'name' cannot be left blank.")
body_attributes.add_argument('stars')
body_attributes.add_argument('daily')
body_attributes.add_argument('city')

def normalize_path_params(city=None, star_min = 0, star_max = 5,
    daily_min = 0, daily_max = 10000, limit = 50, offset = 0, **data):
    if city:
        return {
            'star_min': star_min,
            'star_max': star_max,
            'daily_min': daily_min,
            'daily_max': daily_max,
            'city': city,
            'limit': limit,
            'offset': offset}
    return {
        'star_min': star_min,
        'star_max': star_max,
        'daily_min': daily_min,
        'daily_max': daily_max,
        'limit': limit,
        'offset': offset}

'''
Route for /hotels>
'''
@api.doc(params={
    'star_min': 'Minimal stars',
    'star_max': 'Max stars',
    'daily_min': 'Minimal daily',
    'city': 'City',
    'limit': 'Limit',
    'offset': 'Offset'
})
@hotels_namespace.route('')
class Hotels(Resource):
    # Return all hotels
    def get(self):
        connection = sqlite3.connect('database.db')
        cursor = connection.cursor()

        params = path_params.parse_args()
        valid_data = { key:params[key] for key in params if params[key] is not None }        
        parameters = normalize_path_params(**valid_data)

        if not parameters.get('city'):
            query = "SELECT * FROM hotels \
            WHERE (stars >= ? and stars <= ?) \
            and (daily >= ? and daily <= ?) \
            LIMIT ? OFFSET ?"
            _tuple = tuple([parameters[key] for key in parameters])
            result = cursor.execute(query, _tuple)
        else:
            query = "SELECT * FROM hotels \
            WHERE (stars >= ? and stars <= ?) \
            and (daily >= ? and daily <= ?) \
            and cidade = ? LIMIT ? OFFSET ?"
            _tuple = tuple([parameters[key] for key in parameters])
            result = cursor.execute(query, _tuple)

        hotels = []
        for linha in result:
            hotels.append({
            'hotel_id': linha[0] ,
            'name': linha[1],
            'stars': linha[2],
            'daily': linha[3],
            'city': linha[4]
            })

        return {'hotels': hotels}

'''
Route for /hotels/<string:hotel_id>
'''
@api.doc(params={'hotel_id': 'Hotel ID'})
@hotel_namespace.route('/<string:hotel_id>')
class Hotel(Resource):
    # Get Hotel by id
    @hotel_namespace.marshal_list_with(hotel_model, mask='')
    def get(self, hotel_id):
        # Find Hotel by id
        hotel = HotelModel.find_hotel(hotel_id)
        if hotel:
            return hotel.json(), 200 # Ok
        return {'message': 'Hotel not found.'}, 404 # Not found

    # Post Hotel
    # Necessary JWT
    @jwt_required
    @hotel_namespace.marshal_list_with(hotel_model, mask='') # Output in swagger
    @hotel_namespace.expect(hotel_model, validate=False) # Model in swagger
    def post(self, hotel_id):
        # Checks if it already exists
        if HotelModel.find_hotel(hotel_id):
            return {"message": "Hotel id '{}' already exists.".format(hotel_id)}, 400 # Bad Request

        # Casting request Body data
        data = body_attributes.parse_args()
        # Instantiate a new hotel
        hotel = HotelModel(hotel_id, **data)

        try:
            # Save new Hotel
            hotel.save_hotel()
        except:
            # If theres errors
            return {"message": "An error ocurred trying to create hotel."}, 500 # Internal Server Error

        # If all goes well
        return hotel.json(), 201 # Created

    # Put Hotel
    # Necessary JWT
    @jwt_required
    @hotel_namespace.marshal_list_with(hotel_model, mask='')
    def put(self, hotel_id):
        # Casting request Body data
        data = body_attributes.parse_args()
        # Instantiate a new hotel
        hotel = HotelModel(hotel_id, **data)
        # Find Hotel by id
        hotel_found = HotelModel.find_hotel(hotel_id)

        if hotel_found:
            # Update Hotel
            hotel_found.update_hotel(**data)
            # Save Hotel updated in  DB
            hotel_found.save_hotel()
            return hotel_found.json(), 200 # Ok
        
        # If hotel not found, create a new
        hotel.save_hotel()
        # If all goes well
        return hotel.json(), 201 # Created

    # Delete Hotel
    # Necessary JWT
    @jwt_required
    def delete(self, hotel_id):
        # Find Hotel by id
        hotel = HotelModel.find_hotel(hotel_id)
        if hotel:
            # Delete Hotel
            hotel.delete_hotel()
            return {'message': 'Hotel deleted.'}, 200 # Ok
        return {'message': 'Hotel not found.'}, 404 # Not found
